
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


var sessionId;

//creata arraye za belezenje podatkov
var teza = [];
var visina = [];
var datumRojstva = [];


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}




/////////////////////////////////////////
/**                                   //
 *  GENERIRANJE PACIENTOV            //
 */                                 //
 /**                               //
 * parametri: stPacienta ( 1/2/3) //
 * glede na to nastavimo podatke //
 */                             //
/////////////////////////////////


function generiraj(stPacienta) {
   var  ehrId = "";

  //implementacija
  
  sessionId = getSessionId();
  
  var name;     var lastName;
  var DOB; // DATE OF BIRTH    
  var height;   var weight;
  var meritveArray;
   $.ajaxSetup({
    headers: {
      "Ehr-Session": sessionId
    }
  });
  
  if(stPacienta == 1){
    name = "Kuzma" 
    lastName = "Ivanov"
    DOB = "1960-05-25T6:30"
    height = 160;
    weight = 90;
    
      meritveArray = [
       
        ["1970-03-04T00:01", 130, 40],
        ["1980-08-08T08:08", 170, 55],
        ["1990-11-11T11:12", 170, 60],
        ["2000-12-12T12:12", 168, 70],
        ["2015-04-01T00:00", 160, 90]
      ]
   
  }
  
 else if(stPacienta == 2){
    name = "Varian" 
    lastName = "O'Kendrick"
    DOB = "1990-06-10T21:45"
    height = 190;
    weight = 75;
   
      meritveArray = [
        ["1995-03-04T00:01", 100, 20],
        ["2000-08-08T08:08", 140, 30],
        ["2005-11-11T11:12", 175, 40],
        ["2010-12-12T12:12", 185, 55.5],
        ["2016-04-01T00:00", 190, 65.6]
      ]
  }
  
  else  if(stPacienta == 3){
    name = "Beatrice" 
    lastName = "Caine"
    DOB = "1996-07-24T1:00"
    height = 175;
    weight = 60;
 
      meritveArray = [
        
        ["1997-03-04T00:01", 50, 10],
        ["2000-08-08T08:08", 70, 20],
        ["2005-11-11T11:12", 130, 35],
        ["2010-12-12T12:12", 160, 51.5],
        ["2015-04-01", 177, 63.5]
      ]
  }

  $.ajax({
    url: baseUrl + "/ehr",
    type: 'POST',
    success: function(data) {
      ehrId = data.ehrId;
      var partyData = {
        firstNames: name,
        lastNames: lastName,
        dateOfBirth: DOB,
        partyAdditionalInfo: [{key: "ehrId",  value: ehrId, },
                              {key: "weight", value: weight }, 
                              {key: "height", value: height }]
      };

      $.ajax({
        url: baseUrl + "/demographics/party",
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(partyData),
        success: function(party) {
          if (party.action == 'CREATE') {
            console.log("HI");
            for (var i = 0; i < 5; i++) {
              dodajMeritve(ehrId, meritveArray[i])
            }
            if (stPacienta == 1){
                $("#PacientKuzma").val(ehrId)
            }
            if (stPacienta == 2){
                $("#PacientVarian").val(ehrId)
            }
            if (stPacienta == 3){
                $("#PacientBeatrice").val(ehrId)
            }
          }
          
        },
        error: function(err) {
          alert("Prislo je do napake pri generiranju pacientov: " + JSON.parse(err.responseText).userMessage);
        }
      });
     // return ehrId;
    }
  })
}


function prikaziPodatke(ime_in_priimek, rojen) {
  
  $("#naslovPodatki").html("<strong> Podatki trenutnega pacienta:</strong>"); 
  $("#podatkiOBolniku").html("Ime in priimek: <strong>" + ime_in_priimek + "</strong>");
  $("#rojen").html("Rojen/a: <strong>" + rojen + "</strong>");
  $("#v").html("Višina: <strong>" + visina[5] + "cm</strong>"); 
  $("#t").html("Teža: <strong>" + teza[5] + "kg</strong>"); 
  
  
  var indeks = Math.round ((teza [5] / (visina[5]*0.01 *visina[5]*0.01))*100) / 100;
 
  $("#ITM").html("Trenutni indeks telesne mase: <strong>" + indeks + "</strong>");
  var stanje = 0;
  if (indeks < 18.5) {
     stanje = "podhranjenost"
     $("#opis").html("Trenutno stanje: <strong>" + stanje + "</strong>"); 
     $("#predlog").html("Nasvet: Zadostite osnovne kalorične potrebe telesa vendar se izogibajte prenajedanju. V primeru, da se vam stanje v treh tednih ne izboljša, je priporočljiv posvet z osebnim zdravnikom."); 
  }
   if (indeks > 30) {
     stanje = "debelost"
     $("#opis").html("Trenutno stanje: <strong>" + stanje + "</strong>"); 
     $("#predlog").html("Nasvet: Zmanjšajte zaužitje visoko kaloričnih in predpripravljenih živil ter se vsaj trikrat na teden vsaj šestdeset minut ukvarjajte s športnimi aktivnostmi. V primeru, da se vam stanje v treh tednih ne izboljša, je priporočljiv posvet z osebnim zdravnikom."); 
  }
   if (18.5<indeks && indeks < 30) {
    stanje = "normalni indeks telesne mase"
     $("#opis").html("Trenutno stanje: <strong>" + stanje + "</strong>"); 
     $("#predlog").html("Nasvet: Uživajte v življenju in še naprej ohranjajte aktiven življenski slog."); 
    
  }
}

    // PODOBNO KOT NA PREDAVANJIH
function preberiEHRodBolnika() {
  // Dobi samo osnovne podatke trenutnega bolnika
  $("#preberiSporocilo").html("")

  // prvo stare podatke odstrani, in nalozi nove
  $("#naslovPodatki").html("")
  $("#naslovPodatki").val("")
  $("#podatkiOBolniku").html("")
  $("#podatkiOBolniku").val("")
  $("#rojen").html("")
  $("#rojen").val("")
  $("#v").html("")
  $("#v").val("")
  $("#t").html("")
  $("#t").val("")
  $("#opis").html("")
  $("#opis").val("")
  $("#predlog").html("")
  $("#predlog").val("")
  $("#ITM").html("")
  $("#ITM").val("")
  
  sessionId = getSessionId();
  var ehrId = $("#preberiEHRid").val();
  if (!ehrId || ehrId.trim().length == 0) {
    $("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in' style='margin-left:5px;'>Prosim vnesite zahtevan podatek!");
    $("#graf").html("");
    $("#graf").css({
      'height': '',
      'width': ''
    });
    $("#podatkiOBolniku").html("")
    $("#podatkiOBolniku").val("")
  }
  else {
    $.ajax({
      url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
      type: 'GET',
      headers: {
        "Ehr-Session": sessionId
      },
      success: function(data) {
        var party = data.party;
        prikaziPodatke(data.party.firstNames + " " + data.party.lastNames, data.party.dateOfBirth)
      },
      error: function(err) {
        $("#gumbPredlog").prop("disabled", true)
      }
    });
  }
}


    //PODOBNO KOT NA PREDAVANJIH, posljemo notri array meritve //

function dodajMeritve(ehrId, meritve) {
  // meritve = array
  sessionId = getSessionId();

  var datumInUra = meritve[0]
  var telesnaVisina = meritve[1]
  var telesnaTeza = meritve[2]


  $.ajaxSetup({
    headers: {
      "Ehr-Session": sessionId
    }
  });
  var podatki = {
    // Struktura predloge je na voljo na naslednjem spletnem naslovu:
    // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
    "ctx/language": "en",
    "ctx/territory": "SI",
    "ctx/time": datumInUra,
    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,

  };
  var parametriZahteve = {
    ehrId: ehrId,
    templateId: 'Vital Signs',
    format: 'FLAT',
  };
  $.ajax({
    url: baseUrl + "/composition?" + $.param(parametriZahteve),
    type: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(podatki),
    success: function(res) {},
    error: function(err) {
      alert("Napaka pri generiranju bolnikov: " + JSON.parse(err.responseText).userMessage)
    }

  });
}


function preberiVsePodatke() {
  sessionId = getSessionId();
  var podatki = false;
  var ehrId = $("#preberiEHRid").val();
  if (!ehrId || ehrId.trim().length == 0) {
    $("#preberiSporocilo").html("<span class='obvestilo label label-warning fade-in' style='margin-left:5px;'>Prosim vnesite zahtevan podatek!");
  }
  else {
    $.when(
      // get height
      $.ajax({
        url: baseUrl + "/view/" + ehrId + "/height",
        type: 'GET',
        headers: {"Ehr-Session": sessionId },
        success: function(data) {
          
            for (var i = 0; i < 5; i++) {
             
              visina[5 - i] = data[i].height
            }
           //posodobi graf
            ustvariGraf()
          
        },
        
      }),

      

      // get weight
      $.ajax({
        url: baseUrl + "/view/" + ehrId + "/weight",
        type: 'GET',
        headers: {
          "Ehr-Session": sessionId
        },
        success: function(data) {
          try {
            for (var i = 0; i < 5; i++) {
              
              teza[5 - i] =  data[i].weight
              datumRojstva[5 - i] =  data[i].time
            }
            // update graph here
            ustvariGraf()
          }
          catch (TypeError) {
            podatki = true;
          }
        },
        error: function(err) {
          console.log(err)
        }
      })

    ).then(
      function() {
        if (podatki) {
          alert("Izbran bolnik nima ustreznih podatkov za prikaz.");
          $("#graf").html("");
          $("#graf").css({
            'height': '',
            'width': ''
          });
          
          $("#podatkiOBolniku").html("")
          $("#podatkiOBolniku").val("")
          
          
          return;
        }
      }
     
    );
  }
}





    /* NARISI GRAF  */
      
function ustvariGraf() {
  console.log("2");
  $("#graf").css('height', '400px');
  $("#graf").css('width', '100%');
  var weigh = {
    x: datumRojstva,
    y: teza,
    name: 'Teža (kg)',
    type: 'bar'
  };
  var high = {
    x: datumRojstva,
    y: visina,
    name: 'Višina (cm)',
    type: 'bar'
  };

  var podatki = [weigh, high];
  var layout = {
    xaxis: {
      title: 'Čas'
    },
    yaxis: {
      title: 'Vrednost'
    },
    barmode: 'absolute',
    title: 'Meritve teže in višine',
  };

  Plotly.newPlot('graf', podatki, layout);
}


